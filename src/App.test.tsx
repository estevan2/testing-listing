import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App";

test("renders learn react link", () => {
  render(<App />);
  const linkElement = screen.getByText(/add/i);
  expect(linkElement).toBeInTheDocument();
});

test('Quando o usuário clicar no botão "Enviar" o valor do input deve ser adicionado na lista', () => {
  render(<App />);
  fireEvent.change(screen.getByTestId("input_list_item"), {
    target: { value: "teste" },
  });
  userEvent.click(screen.getByTestId("button_add_item"));
  const listItem = screen.getByText(/teste/i);
  expect(listItem).toBeInTheDocument();
});

test("Quando o valor do input for vazio, o botão deve ficar disabled", () => {
  render(<App />);
  fireEvent.change(screen.getByTestId("input_list_item"), {
    target: { value: "" },
  });
  const button = screen.getByTestId("button_add_item");
  expect(button).toHaveProperty("disabled", true);
});
