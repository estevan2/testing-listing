import React, { useState } from "react";
import "./App.css";

function App() {
  const [list, setList] = useState<string[]>([]);
  const [inputValue, setInputValue] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  const handleClick = () => {
    setList([...list, inputValue]);
  };

  const resetList = () => {
    setList([]);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <input
            onChange={handleChange}
            type="text"
            data-testid="input_list_item"
          />
          <button
            onClick={handleClick}
            disabled={!!!inputValue}
            data-testid="button_add_item"
          >
            Add
          </button>
          <button onClick={resetList} disabled={list.length === 0}>
            reset
          </button>
        </div>
        <ul>
          {list.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </header>
    </div>
  );
}

export default App;
